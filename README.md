This module will add a promotion condition to check for specific term in a 
specific order field, if matches the promotio could be applied.

This module also provides an action to bulk set (in absolute way) the terms of
a field configured in action configuration. It will ask if more terms must be 
added when applying the action.

Combining both one can create promotions applied to orders, an use case may be
adding multiple promotions to existing orders such as press passes e.g.
Another use case is to prevent that order form could expire and the automatic
price adjustment prevents you to edit the order. As the tag set operation is 
faster.
