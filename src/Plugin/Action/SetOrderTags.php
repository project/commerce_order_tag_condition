<?php

declare(strict_types = 1);

namespace Drupal\commerce_order_tag_condition\Plugin\Action;

use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Action\ConfigurableActionBase;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Render\Element\Checkboxes;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a set order tags action.
 *
 * @Action(
 *   id = "commerce_order_tag_condition_set_order_tags",
 *   label = @Translation("Set order tags"),
 *   type = "commerce_order",
 *   category = @Translation("Promotion"),
 *   confirm = TRUE,
 *   confirm_form_route_name = "commerce_order_tag_condition.set_tags_confirm"
 * )
 *
 * @DCG
 * For updating entity fields consider extending FieldUpdateActionBase.
 * @see \Drupal\Core\Field\FieldUpdateActionBase
 *
 * @DCG
 * In order to set up the action through admin interface the plugin has to be
 * configurable.
 * @see https://www.drupal.org/project/drupal/issues/2815301
 * @see https://www.drupal.org/project/drupal/issues/2815297
 *
 * @DCG
 * The whole action API is subject of change.
 * @see https://www.drupal.org/project/drupal/issues/2011038
 */
final class SetOrderTags extends ConfigurableActionBase implements ContainerFactoryPluginInterface {

  /**
   * The tempstore factory.
   *
   * @var \Drupal\Core\TempStore\PrivateTempStoreFactory
   */
  protected $tempStoreFactory;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    private readonly EntityTypeManagerInterface $entityTypeManager,
    private readonly PrivateTempStoreFactory $temp_store_factory
  ) {
    $this->tempStoreFactory = $temp_store_factory;
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): self {
    return new self(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('tempstore.private')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return ['tags' => '', 'vocabularies' => [''], 'destination_term_field' => ''];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    // $form = parent::buildConfigurationForm($form, $form_state);
    if (is_array($this->configuration['tags'])) {
      $term_ids = array_column($this->configuration['tags'], 'target_id');
      $tags = $this->entityTypeManager->getStorage('taxonomy_term')->loadMultiple($term_ids);
    }
    else {
      $tags = "";
    }

    $names = \Drupal::entityQuery('taxonomy_vocabulary')->execute();

    $vocabularies = $form_state->getValue('selected_vocabularies');
    $form['vocabularies'] = [
      '#type' => 'checkboxes',
      '#options' => $names,
      '#default_value' => $this->configuration['vocabularies'],
      '#ajax'    => [
        'callback' => [$this, 'selectVocabulariesAjax'],
        'wrapper'  => 'terms_wrapper',
      ],
    ];

    $form['tags'] = [
      '#type' => 'entity_autocomplete',
      '#title' => $this->t('Tags'),
      '#default_value' => $tags,
      '#target_type' => 'taxonomy_term',
      '#selection_settings' => [
        'target_bundles' => $form_state->getValue('vocabularies') ?: $this->configuration['vocabularies'],
      ],
      '#tags' => TRUE,
        // '#hide_single_entity' => FALSE,
        // '#multiple' => TRUE,
        // '#required' => TRUE,
      '#prefix'    => '<div id="terms_wrapper">',
      '#suffix'    => '</div>',
      '#description' => " " . $this->t('The selected tag will be prepopulated when  applying the orders tags change.'),
    ];

    // @todo List existing taxonomy term fields of all bundles
    $form['destination_term_field'] = [
      '#type' => 'textfield',
      '#default_value' => $this->configuration['destination_term_field'],
      '#required' => TRUE,
      '#description' => $this->t('The machine name of the field that will be used to set the terms'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state): void {
    $this->configuration['vocabularies'] = $form_state->getValue('vocabularies');
    $this->configuration['tags'] = $form_state->getValue('tags');
    $this->configuration['destination_term_field'] = $form_state->getValue('destination_term_field');
  }

  /**
   * {@inheritdoc}
   */
  public function access($entity, AccountInterface $account = NULL, $return_as_object = FALSE): AccessResultInterface|bool {
    /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
    $access = $entity->access('update', $account, TRUE)
      ->andIf($entity->get($this->configuration['destination_term_field'])->access('edit', $account, TRUE));
    return $return_as_object ? $access : $access->isAllowed();
  }

  /**
   * {@inheritdoc}
   */
  public function execute(ContentEntityInterface $entity = NULL): void {
    $this->executeMultiple([$entity]);
  }

  /**
   * {@inheritdoc}
   */
  public function executeMultiple(array $entities) {
    $this->tempStoreFactory->get('orders_operations_set_tags')->set('orders_list', [
      'entities' => $entities,
      'config' => $this->getConfiguration(),
    ]);
  }

  /**
   * Called via Ajax to populate the terms field according vocabulary.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The form terms field structure.
   */
  public function selectVocabulariesAjax(array &$form, FormStateInterface $form_state) {
    $selected_vocabularies = Checkboxes::getCheckedCheckboxes($form_state->getValue('vocabularies'));
    $form['tags']['#description'] = $selected_vocabularies ? $this->t('Select the tag from %vocabularies.', ["%vocabularies" => implode(", ", $selected_vocabularies)]) : "";
    $form['tags']['#description'] .= " " . $this->t('The selected tag will be prepopulated when  applying the orders tags change.');
    return $form['tags'];
  }

}
