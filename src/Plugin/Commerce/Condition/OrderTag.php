<?php

declare(strict_types = 1);

namespace Drupal\commerce_order_tag_condition\Plugin\Commerce\Condition;

use Drupal\commerce\Plugin\Commerce\Condition\ConditionBase;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides the tag condition for orders.
 *
 * @CommerceCondition(
 *   id = "order_tag",
 *   label = @Translation("Order tag"),
 *   category = @Translation("Order", context = "Commerce"),
 *   entity_type = "commerce_order",
 * )
 */
final class OrderTag extends ConditionBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return ['tags' => '', 'checked_term_field' => ''] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildConfigurationForm($form, $form_state);
    // @see https://wengerk.medium.com/how-to-create-an-entity-field-widget-autocomplete-for-internal-data-on-drupal-c06a795a5d66
    /*$form['tags'] = [
    '#type' => 'commerce_entity_select',
    '#title' => $this->t('Tags'),
    '#default_value' => $this->configuration['tags'],
    '#target_type' => 'taxonomy_term',
    '#hide_single_entity' => FALSE,
    '#multiple' => TRUE,
    '#required' => TRUE,
    ];*/
    if (is_array($this->configuration['tags'])) {
      $term_ids = array_column($this->configuration['tags'], 'target_id');
      $tags = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadMultiple($term_ids);
    }
    else {
      $tags = "";
    }
    $form['tags'] = [
      '#type' => 'entity_autocomplete',
      '#title' => $this->t('Tags'),
      '#default_value' => $tags,
      '#target_type' => 'taxonomy_term',
      '#selection_settings' => [
        'target_bundles' => ['promotion_tag'],
      ],
      '#tags' => TRUE,
      // '#hide_single_entity' => FALSE,
      // '#multiple' => TRUE,
      '#required' => TRUE,
    ];

    // @todo List existing taxonomy term fields of all bundles
    $form['checked_term_field'] = [
      '#type' => 'textfield',
      '#default_value' => $this->configuration['checked_term_field'],
      '#required' => TRUE,
      '#description' => $this->t('The machine name of the field that will be checked for terms beign defined'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state): void {
    parent::submitConfigurationForm($form, $form_state);

    $values = $form_state->getValue($form['#parents']);
    $this->configuration['tags'] = $values['tags'];
    $this->configuration['checked_term_field'] = $values['checked_term_field'];
  }

  /**
   * {@inheritdoc}
   */
  public function evaluate(EntityInterface $entity): bool {
    $this->assertEntity($entity);
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $entity;
    foreach ($order->get($this->configuration['checked_term_field'])->referencedEntities() as $ref_ent) {
      if (in_array($ref_ent->id(), array_column($this->configuration['tags'], 'target_id'))) {
        return TRUE;
      }
    }
    return FALSE;
  }

}
