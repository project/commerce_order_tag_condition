<?php

declare(strict_types = 1);

namespace Drupal\commerce_order_tag_condition\Form;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Sets the terms of a given order field.
 */
final class SetTagsConfirmForm extends ConfirmFormBase {

  /**
   * The temp store factory.
   *
   * @var \Drupal\Core\TempStore\PrivateTempStoreFactory
   */
  protected $tempStoreFactory;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new UserMultipleCancelConfirm.
   *
   * @param \Drupal\Core\TempStore\PrivateTempStoreFactory $temp_store_factory
   *   The temp store factory.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(PrivateTempStoreFactory $temp_store_factory, EntityTypeManagerInterface $entity_type_manager) {
    $this->tempStoreFactory = $temp_store_factory;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('tempstore.private'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'commerce_order_tag_condition_set_tags_confirm';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion(): TranslatableMarkup {
    return $this->t('Which tags you want to set?');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Retrieve the entities to add tags from the temp store.
    ['entities' => $orders, 'config' => $config] = $this->tempStoreFactory
      ->get('orders_operations_set_tags')
      ->get('orders_list');
    if (!$orders && !$config) {
      return $this->redirect('entity.commerce_order.collection');
    }
    $labels = [];
    $form['orders'] = ['#tree' => TRUE];
    foreach ($orders as $order) {
      $order_id = $order->id();
      $labels[$order_id] = $order->label();

      $form['orders'][$order_id] = [
        '#type' => 'hidden',
        '#value' => $order_id,
      ];
    }

    $form_state->set('destination_term_field', $config['destination_term_field']);

    $form['order']['labels'] = [
      '#theme' => 'item_list',
      '#items' => $labels,
    ];

    $form['operation'] = ['#type' => 'hidden', '#value' => 'cancel'];

    if (is_array($config['tags'])) {
      $term_ids = array_column($config['tags'], 'target_id');
      $tags = $this->entityTypeManager->getStorage('taxonomy_term')->loadMultiple($term_ids);
    }
    else {
      $tags = "";
    }
    $form['tags'] = [
      '#type' => 'entity_autocomplete',
      '#title' => $this->t('Tags'),
      '#default_value' => $tags,
      '#target_type' => 'taxonomy_term',
      '#selection_settings' => [
        'target_bundles' => $config['vocabularies'],
      ],
      '#tags' => TRUE,
      '#description' => $this->t('The absolute terms in order'),
      // '#hide_single_entity' => FALSE,
      // '#multiple' => TRUE,
      // '#required' => TRUE,
    ];

    $form = parent::buildForm($form, $form_state);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl(): Url {
    return new Url('entity.commerce_order.collection');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    foreach ($form_state->getValue('orders') as $order_id => $order_form_value) {
      $order = $this->entityTypeManager->getStorage('commerce_order')->load($order_id);
      $order->set($form_state->get('destination_term_field'), $form_state->getValue('tags'))->save();
    }

    $this->messenger()->addStatus($this->t('Promotion tags applied!'));
    $form_state->setRedirectUrl(new Url('entity.commerce_order.collection'));
  }

}
